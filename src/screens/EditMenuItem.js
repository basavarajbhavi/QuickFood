import React, { Component } from "react";
import { editItem } from "../ServiceLayer/Restaurant";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import "react-confirm-alert/src/react-confirm-alert.css";
import Swal from "sweetalert2";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "bootstrap/dist/css/bootstrap.css";

import "../App.css";

class EditMenuItem extends Component {
  constructor() {
    super();
    this.state = {};
    this.handleItemImage = this.handleItemImage.bind(this);
    this.handleEditYourItemBtn = this.handleEditYourItemBtn.bind(this);
  }
  // get the menuitems
  async componentDidMount() {
    let menuItem = {};
    var token = localStorage.getItem("securityToken");
    const options = {
      url:
        process.env.REACT_APP_NodeJS_ServiceURL +
        `/api/v1/menuitems/item/${this.props.itemId}`,
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
    };
    await axios(options)
      .then((response) => {
        console.log('dta',response.data.data);
        menuItem = response.data.data;
        console.log('menuitem id',menuItem.itemId)
      })
      .catch((error) => {
        console.log(error);
      });
    this.setState({
      itemImageLable: menuItem.itemImageLable,
      itemTitle: menuItem.itemTitle,
      itemIngredients: menuItem.itemIngredients,
      itemPrice: menuItem.itemPrice,
      itemImage: menuItem.itemImageUrl,
      chooseItemType: menuItem.chooseItemType,
      showError: false,
      registerFormError: "",
      showModal: this.props.showModal,
      menuItemId: this.props.itemId,
    });
  }
  //Handleclose is for closing the dialog box
  handleClose = () => this.props.handleClose(false);
  handleItemImage(e) {
    if (e.target.files[0] != null) {
      this.setState({
        itemImageLable: e.target.files[0].name,
        itemImage: e.target.files[0],
      });
    } else {
      this.setState({
        itemImageLable: "Choose image",
        itemImage: "",
      });
    }
  }
  // Confirm dialog box
   handleConfirmBox(addItemReturn) {
     console.log("Confirm alert");
     this.handleClose();
     confirmAlert({
       title: "Confirm to submit",
       message: "Do you want to update this food item in the list.",
       buttons: [
         {
           label: "Yes",
           onClick: () =>
             Swal.fire({
               title: "Success",
               text: addItemReturn,
               type: "success",
             }).then(() => {
              //this.props.history.push("/my-foods");
              this.handleClose();
               window.location.reload(false);
             }),
         },
         {
           label: "No",
           onClick: () => this.handleClose(),
         },
       ],
     });
  }

  //On click Save button
  async handleEditYourItemBtn() {
    const {
      itemTitle,
      itemIngredients,
      itemPrice,
      itemImage,
      chooseItemType,
      menuItemId,
    } = this.state;
    if (!itemTitle) {
      this.setState({
        showError: true,
        registerFormError: "Invalid item title.",
      });
    } else if (!itemIngredients) {
      this.setState({
        showError: true,
        registerFormError: "Invalid item ingredients.",
      });
    } else if (!itemPrice) {
      this.setState({
        showError: true,
        registerFormError: "Invalid item price.",
      });
    } else if (!chooseItemType) {
      this.setState({
        showError: true,
        registerFormError: "Must be selected any one.",
      });
    } else {
      this.setState({
        showError: false,
        registerFormError: "",
      });
      const itemDetails = {
        menuItemId,
        itemTitle,
        itemIngredients,
        itemPrice,
        itemImage,
        chooseItemType,
        propsHistory: this.props.history,
      };

      try {
        const addItemReturn = await editItem(itemDetails);
        Swal.fire({
          title: "success",
          text: "Update successfully",
          type: "success",
        });
      
        
      } catch (error) {
        Swal.fire({
          title: "Error",
          text: error,
          type: "error",
        });
      }
    }
  }

  //On click clear button
  handleClearYourItemBtn(e) {
    /*Array.from(document.querySelectorAll("input")).forEach(
      (input) => (input.value = " ")
    );*/
    this.setState({
      chooseItemType: "",
      itemTitle: "",
      itemIngredients: "",
      itemPrice: "",
    });
  }

  render() {
    const { itemImageLable, showError, registerFormError } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.state.showModal}
          dialogClassName="modal-90w"
          onHide={this.handleClose}
        >
          {
            <Modal.Header closeButton>
              <Modal.Title>Edit Menu Item</Modal.Title>
            </Modal.Header>
          }
          <Modal.Body size="lg">
            <div className="col-lg-12 col-md-12 col-sm-12 mx-auto">
              <form action="javascript:void(0)">
                <div
                  className="form-row"
                  style={{ display: "flex", flexDirection: "row" }}
                >
                  <div className="col-md-6 col-lg-6">
                    <div className="form-group col-md-12 col-lg-12">
                      <label htmlFor="itemTitle">
                        <b>Item Title</b>
                      </label>
                      <input
                        value={this.state.itemTitle}
                        type="text"
                        className="form-control"
                        id="itemTitle"
                        placeholder="Full name of dish"
                        onChange={(e) =>
                          this.setState({ itemTitle: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-12 col-lg-12">
                      <label htmlFor="itemIngredients">
                        <b>Item Ingredients</b>
                      </label>
                      <input
                        value={this.state.itemIngredients}
                        type="text"
                        className="form-control"
                        id="itemIngredients"
                        placeholder="Item Ingredients Name"
                        onChange={(e) =>
                          this.setState({ itemIngredients: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <label htmlFor="itemPrice">
                        <b>Price</b>
                      </label>
                      <input
                        value={this.state.itemPrice}
                        type="number"
                        className="form-control"
                        id="itemPrice"
                        placeholder="Price in number"
                        onChange={(e) =>
                          this.setState({ itemPrice: e.target.value })
                        }
                      />
                    </div>
                  </div>
                  <div
                    className="col-lg-6 col-md-6 col-6"
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <img
                      style={{ width: "15em", height: "15em" }}
                      alt="Natural Healthy Food"
                       src={this.state.itemImage}
                    />
                  </div>
                  {/* <div className="form-group col-md-6">
                                    <label className="mb-2"><b>Item Image</b></label>
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="itemImage" accept="image/*" onChange={this.handleItemImage} />
                                        <label value={this.state.itemImage} className="custom-file-label" htmlFor="itemImage">{itemImageLable}</label>
                                    </div>
                                </div> */}
                </div>
                <div className="col-lg-12 col-md-12 col-12">
                  <label className="mb-2">
                    <b>Choose Item Type</b>
                  </label>
                  <div className="form-row">
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Kebabs"
                          value="Kebabs"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Kebabs"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Kebabs"
                        >
                          Kebabs
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Chicken"
                          value="Chicken"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Chicken"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Chicken"
                        >
                          Chicken
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Burgers"
                          value="Burgers"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Burgers"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Burgers"
                        >
                          Burgers
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Biryani"
                          value="Biryani"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Biryani"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Biryani"
                        >
                          Biryani
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Sauces"
                          value="Sauces"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Sauces"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Sauces"
                        >
                          Sauces
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        {console.log("Before veg",this.state.chooseItemType)}
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Vegetarian"
                          value="Vegetarian"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Vegetarian"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Vegetarian"
                        >
                          Vegetarian
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Bread"
                          value="Bread"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Bread"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label className="custom-control-label" htmlFor="Bread">
                          Bread
                        </label>
                      </div>
                    </div>
                    <div className="form-group col-md-3">
                      <div className="custom-control custom-radio">
                        <input
                          type="radio"
                          className="custom-control-input"
                          id="Specials"
                          value="Specials"
                          name="chooseItemType"
                          checked={this.state.chooseItemType === "Specials"}
                          onChange={(e) =>
                            this.setState({ chooseItemType: e.target.value })
                          }
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="Specials"
                        >
                          Specials
                        </label>
                      </div>
                    </div>
                  </div>
                  {showError ? (
                    <p className="text-danger">{registerFormError}</p>
                  ) : null}
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={this.handleEditYourItemBtn}
                  >
                    <b>Save</b>
                  </button>
                  {"  "}
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={(e) => this.handleClearYourItemBtn(e)}
                  >
                    <b>Clear</b>
                  </button>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12"></div>
              </form>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
export default EditMenuItem;
