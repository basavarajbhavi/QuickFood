import React, { Component } from 'react';
import Navbar2 from '../components/Navbar2';
import Footer from '../components/Footer';

import 'bootstrap/dist/css/bootstrap.css';
import '../App.css';
import '../Styles/contact.css';
import Swal from 'sweetalert2'
//import { faHandPointDown } from '@fortawesome/free-solid-svg-icons';
//import { Redirect } from 'react-router-dom';

var CoverPic = require('../assets/images/bg-01.jpg');

export default class ContactUs extends Component {
    constructor() {
        super()
        this.state = {
            fullname: "",
            email: "",
            phonenumber: "",
            message: "",
            // showErrorEmail: false,
            // showErrorfullname: false,
            // showErrorPhone: false,
            // showErrorMessage: false,
        
        }
        
        this.handlecontact = this.handlecontact.bind(this);
       
      
    }

    handleFullName(e) {
        const fullname = e;
        const userNameFormate = /^([A-Za-z.\s_-]).{2,}$/;
        if(fullname.match(userNameFormate)){
            this.setState({
                showErrorfullname: false,
                registerFormErrorFullname: null,
                fullname: fullname,
            });

        }
        else{
            this.setState({
                showErrorfullname: true,
                registerFormErrorFullname: "Please enter the valid name",
                fullname: "",
            });

        }
    }
        handleEmail(e) {
            const email = e;
            const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          
            if(email.match(userEmailFormate)){
                this.setState({
                    showErrorEmail: false,
                    registerFormErrorEmail: "",
                    email: email,
                });
    
            }
            else{
                this.setState({
                    showErrorEmail: true,
                    registerFormErrorEmail: "Please enter the valid email address",
                    email: "",
                });
    
            }      
        }
        handlePhoneNumber(e) {
            const phonenumber = e;
            const userPhoneNumberFormate = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
            if(phonenumber.match(userPhoneNumberFormate)){
                this.setState({
                    showErrorPhone: false,
                    registerFormErrorPhone: "",
                    phonenumber: phonenumber,
                });
    
            }
            else{
                this.setState({
                    showErrorPhone: true,
                    registerFormErrorPhone: "Please enter the valid phone number",
                    phonenumber: "",
                });
    
            }      
        }

        handleMessage(e) {
            const message = e;
            const usermessageFormate = /^([A-Za-z.\s_-]).{2,}$/;
            if(message.match(usermessageFormate)){
                this.setState({
                    showErrorMessage: false,
                    registerFormErrorMessage: "",
                    message: message,
                });
    
            }
            else{
                this.setState({
                    showErrorMessage: true,
                    registerFormErrorMessage: "Message is required",
                    message: "",
                });
    
            }      
        }






    async handlecontact() {


        const {fullname, email, phonenumber, message,showErrorEmail,showErrorPhone,showErrorMessage,showErrorfullname} = this.state;
        const userNameFormate = /^([A-Za-z.\s_-]).{2,}$/;
        const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const userPhoneNumberFormate = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
        const usermessageFormate = /^([A-Za-z.\s_-]).{2,}$/;

        if (!fullname.match(userNameFormate)) {
            this.setState({
                showErrorfullname: true,
                registerFormErrorFullname: "Please enter the valid name",
                
            });
            if (!email.match(userEmailFormate)) {
                this.setState({
                    showErrorEmail: true,
                    registerFormErrorEmail: "Please enter the valid email address",
                    
                });
                if(!phonenumber.match(userPhoneNumberFormate)) {
                    this.setState({
                        showErrorPhone: true,
                        registerFormErrorPhone: "Please enter the valid phone number",
                        
                    });
                    if (!message.match(usermessageFormate)) {
                        this.setState({
                            showErrorMessage: true,
                            registerFormErrorMessage: "Message is required",
                    
                        });
                        

                    }
                }
            }
        } 
        if (!email.match(userEmailFormate)) {
            this.setState({
                showErrorEmail: true,
                registerFormErrorEmail: "Please enter the valid email address",
                
            });
            if(!phonenumber.match(userPhoneNumberFormate)) {
                this.setState({
                    showErrorPhone: true,
                    registerFormErrorPhone: "Please enter the valid phone number",
                    
                });
                if (!message.match(usermessageFormate)) {
                    this.setState({
                        showErrorMessage: true,
                        registerFormErrorMessage: "Message is required",
                        
                    });
                    

                }
            }
        }
        if(!phonenumber.match(userPhoneNumberFormate)) {
            this.setState({
                showErrorPhone: true,
                registerFormErrorPhone: "Please enter the valid phone number",
                
            });
            if (!message.match(usermessageFormate)) {
                this.setState({
                    showErrorMessage: true,
                    registerFormErrorMessage: "Message is required",
                    
                });
                

            }
        }
      
        else if(showErrorfullname === false && showErrorEmail === false && showErrorPhone === false && showErrorMessage === false){
            Swal.fire({
                title: 'Success',
                type: 'success',
            }).then(() => {
                this.props.history.push('/')
            })

        }
    
  
    

    }


    render() {
        const {showErrorfullname,showErrorEmail, showErrorPhone, showErrorMessage,registerFormErrorFullname,registerFormErrorEmail,registerFormErrorPhone,registerFormErrorMessage} = this.state;
        return (
            <div>
                <div className="container-fluid res-details-cont1">
                    <div className="">
                        <Navbar2 history={this.props.history} />
                    </div>
                </div>
                <div style={{ background: "#EBEDF3" }} className="container-fluid py-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 bg-white p-4">
                                <h2 className="text-center">Contact Us</h2>
                                <div className="container-contact100">
                                    <div className="wrap-contact100">
                                        <div className="contact100-form-title" style={{ backgroundImage: "url(" + CoverPic + ")" }}>
                                            <span className="contact100-form-title-1">
                                                Contact us
                                                </span>
                                            <span className="contact100-form-title-2">
                                                Feel free to drop us a line below!
                                                </span>
                                        </div>
                                        <form className="contact100-form " action="javascript:void(0)">
                                            <div className="wrap-input100">
                                                <span className="label-input100">Full Name:</span>
                                                <input className="form-control" type="text" name="fullname" placeholder="Enter full name" onKeyUp={(e) =>this.handleFullName(e.target.value)} />
                                                
                                                {showErrorfullname ? <small className="text-danger">{registerFormErrorFullname}</small> : null}
                                            </div>
                                        

                                            <div className="wrap-input100">
                                                <span className="label-input100">Email:</span>
                                                <input className="form-control" type="text" name="email" placeholder="Enter email addess" onKeyUp={(e) => this.handleEmail(e.target.value)}/>
                                                <span className="focus-input100"></span>
                                                {showErrorEmail ? <small className="text-danger">{registerFormErrorEmail}</small> : null}
                                            </div>

                                            <div className="wrap-input100">
                                                <span className="label-input100">Phone:</span>
                                                <input className="form-control" type="text" name="phone" placeholder="Enter phone number"  onKeyUp={(e) => this.handlePhoneNumber(e.target.value)} />
                                                <span className="focus-input100"></span>
                                                {showErrorPhone ? <small className="text-danger">{registerFormErrorPhone}</small> : null}
                                            </div>

                                            <div className="wrap-input100 " >
                                                <span className="label-input100">Message:</span>
                                                <textarea className="form-control" rows="4" name="message" placeholder="Your Comment..." onKeyUp={(e) => this.handleMessage(e.target.value)}></textarea>
                                                <span className="focus-input100"></span>
                                                {showErrorMessage ? <small className="text-danger">{registerFormErrorMessage}</small> : null}
                                            </div>

                                            <div>
                                                <button type="submit" className="btn btn-sm btn-success" onClick={this.handlecontact}>Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div >
        );
    }
}


