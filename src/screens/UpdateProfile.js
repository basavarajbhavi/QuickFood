import React, { Component } from "react";
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
import { connect } from "react-redux";
import { restaurant_list } from "../ServiceLayer/Restaurant";
import RegisterRestaurant from "./RegisterRestaurant";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";
import axios from "axios";

import "bootstrap/dist/css/bootstrap.css";

import "../App.css";

class UpdateProfile extends Component {
  render() {
    return (
      <div>
        <div className="container-fluid res-details-cont1">
          <div className="">
            <Navbar2 history={this.props.history} />
          </div>
        </div>
        <div
          className="form-row"
          style={{ display: "flex", flexDirection: "row" }}
        >
          <div className="col-md-6 col-lg-6">
            <div className="form-group col-md-12 col-lg-12">
              <label htmlFor="restaurantTitle">
                <b>Restaurant Name</b>
              </label>
              <input
                //value={this.state.itemTitle}
                type="text"
                className="form-control"
                id="restaurantTitle"
                placeholder="Restaurant Name"
                onChange={(e) =>
                  this.setState({ restaurantTitle: e.target.value })
                }
              />
            </div>
            <div className="form-group col-md-12 col-lg-12">
              <label htmlFor="email">
                <b>Email</b>
              </label>
              <input
                //value={this.state.itemIngredients}
                type="text"
                className="form-control"
                id="email"
                placeholder="Email"
                onChange={(e) => this.setState({ email: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="phoneNumber">
                <b>Phone Number</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="phoneNumber"
                placeholder="Phone number"
                onChange={(e) => this.setState({ phoneNumber: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="age">
                <b>Age</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="age"
                placeholder="Age"
                onChange={(e) => this.setState({ age: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="address">
                <b>Address</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="address"
                placeholder="Address"
                onChange={(e) => this.setState({ address: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="city">
                <b>City</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="city"
                placeholder="City"
                onChange={(e) => this.setState({ city: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="phoneNumber">
                <b>Country</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="country"
                placeholder="Country"
                onChange={(e) => this.setState({ country: e.target.value })}
              />
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="pincode">
                <b>Pincode</b>
              </label>
              <input
                //value={this.state.itemPrice}
                type="text"
                className="form-control"
                id="pincode"
                placeholder="Pincode"
                onChange={(e) => this.setState({ pincode: e.target.value })}
              />
            </div>
          </div>
          <div
            className="col-lg-6 col-md-6 col-6"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <img
              style={{ width: "15em", height: "15em" }}
              alt="Natural Healthy Food"
              //src={this.state.itemImage}
            />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
export default UpdateProfile;
