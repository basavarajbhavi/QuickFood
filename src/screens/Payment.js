import React, { Component } from "react";
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
import axios from "axios";
import { orderNow } from "../ServiceLayer/Order";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import creditpic from "../assets/images/cc.png";
import paypalpic from "../assets/images/paypal.png";
import cod from "../assets/images/COD.png";

class Payment extends Component {
  constructor() {
    super();
    this.state = {
      usercvv: "",
      registerFormErrorcvv: "",
      registerErrorCardNumber: "",
      showErrorPincvv: false,
      showerrorcardnumber: false,
      usercardnumber: "",
      value: "",
      showErrorCardName: "",
      cardname: "",
      userEmail: "",
      showErrorAddress: false,
      AddressError: "",
      Address: "",


      choosecardtype: "",
      renderCreditCard: false,
      renderPaypalCard: false,
      renderCahOnDelivery: false,
    };
    this.handleUserCardName = this.handleUserCardName.bind(this);
    this.submitPaypal = this.submitPaypal.bind(this);
    this.handleCreditCard = this.handleCreditCard.bind(this);
    this.handleconfirmButton = this.handleconfirmButton.bind(this);
  }

  async componentDidMount() {
    this.setState({
      chooseCardType: "",
    });
  }

  static getDerivedStateFromProps(props) {
    const { state } = props.location;
    const { user } = props;
    return {
      resDetails: state,
      userDetails: user,
    };
  }

  nextPendingScreen() {
    const { userDetails } = this.state;
    if (userDetails) {
      if (!userDetails.isRestaurant) {
        try {
          const history = this.props.history;
          Swal.fire({
            title: "Success",
            text: "Payment Successfull",
            type: "success",
          }).then(() => {
            history.push("/my-orders");
          });
        } catch (error) {
          // console.log(" Error in confirm order => ", error)
          Swal.fire({
            title: "Error",
            text: error,
            type: "error",
          });
        }
      } else {
        // console.log("You are not able to order")
        Swal.fire({
          title: "Error",
          text: "You are not able to pay",
          type: "error",
        });
      }
    }
  }

  confirmPaymentBtn() {}
  handlecvv(e) {
    const usercvv = e;
    if (usercvv.length === 3) {
      this.setState({
        showErrorcvv: false,
        registerFormErrorcvv: "",
      });
    } else {
      this.setState({
        showErrorPincvv: true,
        registerFormErrorcvv: "Please enter a valid 3 digit CVV.",
        usercvv: "",
      });
    }
  }
  handlecardnumber(e) {
    const usercardnumber = e;
    if (usercardnumber.length === 16) {
      this.setState({
        showerrorcardnumber: false,
        registerFormErrorcardNumber: "",
      });
    } else {
      this.setState({
        showerrorcardnumber: true,
        registerFormErrorcardNumber: "Please enter a valid 16 digit Number.",
        usercardnumber: "",
      });
    }
  }

  handleUserEmail(e) {
    const userEmail = e;
    const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (userEmail.match(userEmailFormate)) {
      this.setState({
        showErrorEmail: false,
        registerFormErrorEmail: "",
        userEmail: userEmail,
      });
    } else {
      this.setState({
        showErrorEmail: true,
        registerFormErrorEmail: "Please enter a valid email address.",
        userEmail: "",
      });
    }
  }

  submitPaypal(){
    const {userEmail} = this.state;
    const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!userEmail.match(userEmailFormate)){
      this.setState({
        showErrorEmail: true,
        registerFormErrorEmail: "Please enter a valid email address.",
        userEmail: "",
      });
    }
    else{
      this.props.history.push("/my-orders")
    }
  }
  handleCreditCard(){
    const {cardname, usercardnumber,usercvv} = this.state;
    if(!cardname){
      this.setState({
        cardname: "",
        showErrorCardName: true,
        cardnameError: "Enter the Card Holder Name"
      })

    }
    else if(!usercardnumber.length == 16){
      this.setState({
        showerrorcardnumber: true,
        registerFormErrorcardNumber: "Please enter a valid 16 digit Number.",
        usercardnumber: "",
      });

    }
    else if(!usercvv.length == 3){
      this.setState({
        showErrorPincvv: true,
        registerFormErrorcvv: "Please enter a valid 3 digit CVV.",
        usercvv: "",
      });

    }
    else{
      this.props.history.push("/my-orders")

    }
    
  }
  handleUserCardName(e){
    const cardname = e;
    const usercardName = /^([A-Za-z.\s_-]).{2,}$/;
    if(cardname.match(usercardName)){
      this.setState({
        cardname: cardname,
        showErrorCardName: false,
        cardnameError: ""
      })
    }
    else{
      this.setState({
        cardname: "",
        showErrorCardName: true,
        cardnameError: "Enter the Card Holder Name"
      })
    }
  }
  handleUserCODuser(e){
    const cardname = e;
    const usercardName = /^([A-Za-z.\s_-]).{2,}$/;
    if(cardname.match(usercardName)){
      this.setState({
        cardname: cardname,
        showErrorCardName: false,
        cardnameError: ""
      })
    }
    else{
      this.setState({
        cardname: "",
        showErrorCardName: true,
        cardnameError: "Enter the User Name"
      })
    }
  }
  handleAddress(e){
    const Address = e;
    const userAddress = /^([A-Za-z.\s_-]).{2,}$/;
    if(Address.match(userAddress)){
      this.setState({
        Address: Address,
        showErrorAddress: false,
        AddressError: ""
      })
    }
    else{
      this.setState({
        Address: "",
        showErrorAddress: true,
        AddressError: "Enter the Address"
      })
    }

  }

  handleChooseCardType(event) {
    const name = event.target.value;
    console.log("name", event.target.value);
    if (name === "creditcard") {
      this.setState({
        renderCreditCard: true,
        renderPaypalCard: false,
        renderCahOnDelivery: false,
      });
      this._render_creditcard();
    } else if (name === "paypal") {
      this.setState({
        renderCreditCard: false,
        renderPaypalCard: true,
        renderCahOnDelivery: false,
      });
      this._render_paypal();
    } else {
      this.setState({
        renderCreditCard: false,
        renderPaypalCard: false,
        renderCahOnDelivery: true,
      });
    }
    this._render_cashOnDelivery();
  }

  //Handle the choose card type
  handleChooseCardType(event) {
    const name = event.target.value;
    console.log("name", event.target.value);
    if (name === "creditcard") {
      this.setState({
        renderCreditCard: true,
        renderPaypalCard: false,
        renderCahOnDelivery: false,
      });
      this._render_creditcard();
    } else if (name === "paypal") {
      this.setState({
        renderCreditCard: false,
        renderPaypalCard: true,
        renderCahOnDelivery: false,
      });
      this._render_paypal();
    } else {
      this.setState({
        renderCreditCard: false,
        renderPaypalCard: false,
        renderCahOnDelivery: true,
      });
    }
    this._render_cashOnDelivery();
  }
  handleconfirmButton(){
     const { cardname, Address }= this.state;

    if(!cardname){
      this.setState({
        cardname: "",
        showErrorCardName: true,
        cardnameError: "Enter the User Name"
      })
    } 
    else if(!Address){
      this.setState({
        Address: "",
        showErrorAddress: true,
        AddressError: "Enter the Address"
      })
    }
    else{
      this.props.history.push("/my-orders")
    }
  }

  // Render Credit Card details
  _render_creditcard() {
    const {
      showErrorPincvv,
      showErrorCardName,
      cardnameError,
      registerFormErrorcvv,
      showerrorcardnumber,
      registerFormErrorcardNumber,
    } = this.state;
    return (
      
      <div className="card " >
        <div className="row" style={{margin:"-1px", marginTop:"10px"}}>
          <div className="col">
            <label htmlFor="cardHolder">
              <b>Card Name :</b>
            </label>
            <input
              type="text"
              className="form-control"
              id="name"
              placeholder="Enter Name"
              onChange={(e) => this.handleUserCardName(e.target.value)}
          
            />
             {showErrorCardName? <small className="text-danger mb-0">{cardnameError}</small> : null}
          </div>
         
          <div className="col">
            <label htmlFor="cardHolder">
              <b>Card Number :</b>
            </label>
            <input
              type="number"
              className="form-control"
              id="cardnumber"
              placeholder="Card Number"
              
              onChange={(e) => this.handlecardnumber(e.target.value)}
            />
             {showerrorcardnumber ? (
          <small className="text-danger mb-0">
            {registerFormErrorcardNumber}
          </small>
        ) : null}
          </div>
        </div>
       
        <div className="row" style={{ margin: "10px" }}>

        <label htmlFor="cardHolder">
          <b>CVV :</b>
        </label>
        <input
          type="number"
          className="form-control"
          id="cvv"
          placeholder="Enter CVV Number"
          
          onChange={(e) => this.handlecvv(e.target.value)}
          />
          {showErrorPincvv ? (
          <small className="text-danger mb-0">{registerFormErrorcvv}</small>
        ) : null}
          </div>
        
        <div className="row" style={{ margin: "10px" }}>
        <label htmlFor="cardHolder">
          <b>Expiry Date :</b>
        </label>
        <div className="row ml-1 " >
          <div className="col">
            <input
              className="form-control"
              type="number"
              max="2"
              className="form-control"
              id="expirydate"
              placeholder="MM/YYYY"
              required
            />{" "}
          </div>
          <div className="col">
            <input
              type="number"
              max="2"
              className="form-control"
              id="expirydate"
              size="4"
              placeholder="MM/YYYY"
              required
            />
          </div>
        </div>
        
          <button
            type="submit"
            className="btn btn-warning text-uppercase mb-3"
            style={{ marginTop: "20px" }}
            onClick={this.handleCreditCard}
          >
            <b>Confirm payment</b>
          </button>
         
          </div>
      </div>
    );
  }

  //Render Paypal Card details
  _render_paypal() {
    const { showErrorEmail, registerFormErrorEmail } = this.state;
    return (
      
      <div className="card" style={{padding:"10px"}}>
        <label htmlFor="userEmail">
          <b>Email :</b>
        </label>
        <input
          type="email"
          autoComplete="false"
          className="form-control"
          id="userEmail"
          placeholder="Email"
          required
          onKeyUp={(e) => this.handleUserEmail(e.target.value)}
        />
        {showErrorEmail ? (
          <small className="text-danger mb-0">{registerFormErrorEmail}</small>
        ) : null}
          <button
            type="submit"
            className="btn btn-warning text-uppercase mb-3"
            style={{ marginTop: "20px" }}
            onClick={this.submitPaypal}
          >
            <b>Confirm payment</b>
          </button>
      
      
      </div>
    
    );
  }

  //Render Cash On Delivery card
  _render_cashOnDelivery() {
    const {
      showErrorCardName,
      cardnameError,
      showErrorAddress,
      AddressError,
    } = this.state;
    return (
      <div className="card" style={{padding:"10px"}}>
        <label htmlFor="cardHolder">
          <b>Name :</b>
        </label>
        <input
          type="text"
          className="form-control"
          id="username"
          placeholder="Enter your name"
          max="40"
          style={{ marginBottom: "10px" }}
          onChange={(e) => this.handleUserCODuser(e.target.value)}
        />
         {showErrorCardName? <small className="text-danger mb-0">{cardnameError}</small> : null}

        <label htmlFor="cardHolder">
          <b>Address :</b>
        </label>
        <input
          type="text"
          className="form-control"
          id="address"
          placeholder="Enter Shipping Address"
          max="40"
          onChange={(e) => this.handleAddress(e.target.value)}
        />
        {showErrorAddress? <small className="text-danger mb-0">{AddressError}</small> : null}

       
          <button
            type="submit"
            className="btn btn-warning text-uppercase mb-3"
            style={{ marginTop: "20px" }}
            onClick={this.handleconfirmButton}
          >
            <b>Confirm payment</b>
          </button>
       
      </div>
    );
  }

  //On clicking Confirm payment
  confirmPaymentBtn() {}

  render() {
    const {
      renderCahOnDelivery,
      renderCreditCard,
      renderPaypalCard,
    } = this.state;
    return (
      <div>
        <div className="container-fluid res-details-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />
            <div className="container px-0 res-details-cont1-text mx-0"></div>
          </div>
        </div>
        <div class="card">
          <h5 class="card-header">Payment Details</h5>
          <div class="card-body">
            <div className="row">
              <div className="col">
                <h5 class="card-title">Choose payment type</h5>
                <p class="card-text">
                  <div class="custom-control custom-radio">
                    <input
                      type="radio"
                      class="custom-control-input"
                      id="creditcard"
                      name="chooseCardType"
                      value="creditcard"
                      //checked={this.state.chooseCardType === "creditcard"}
                      onChange={(e) => this.handleChooseCardType(e)}
                    />
                    <label class="custom-control-label" for="creditcard">
                      Credit card
                    </label>
                    <img src={creditpic} width="5%" />
                  </div>
                  <div class="custom-control custom-radio">
                    <input
                      type="radio"
                      id="paypal"
                      class="custom-control-input"
                      name="chooseCardType"
                      value="paypal"
                      //checked={this.state.chooseCardType === "paypal"}
                      onChange={(e) => this.handleChooseCardType(e)}
                    />
                    <label class="custom-control-label" for="paypal">
                      Paypal
                    </label>
                    <img src={paypalpic} width="5%" />
                  </div>
                  <div class="custom-control custom-radio">
                    <input
                      type="radio"
                      id="cashOnDelivery"
                      class="custom-control-input"
                      name="chooseCardType"
                      value="cashOnDelivery"
                      //checked={this.state.chooseCardType === "cashOnDelivery"}
                      onChange={(e) => this.handleChooseCardType(e)}
                    />
                    <label class="custom-control-label" for="cashOnDelivery">
                      Cash on delivery
                    </label>
                    <img src={cod} width="5%" />
                  </div>
                </p>
                <Link to="/shipping-details">
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                  >
                    <b>Back</b>
                  </button>
                </Link>{" "}
              </div>
              <div className="col">
                {renderCreditCard && this._render_creditcard()}
                {renderPaypalCard && this._render_paypal()}
                {renderCahOnDelivery && this._render_cashOnDelivery()}
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps, null)(Payment);
