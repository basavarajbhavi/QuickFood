import React, { Component } from "react";
// import Navbar from '../components/Navbar';
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
// import { signUp } from '../config/firebase';
import { signUp } from "../ServiceLayer/User";
//import { MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

export default class RegisterRestaurant extends Component {
  constructor() {
    super();
    //let { Multiselect } = ReactWidgets;
    this.state = {
      registerFormError: "",
      registerFormErrorUsername: "",
      registerFormErrorEmail: "",
      registerFormErrorPassword: "",
      registerFormErrorCPassword: "",
      registerFormErrorCity: "",
      registerFormErrorAddress: "",
      registerFormErrorPincode: "",
      registerFormErrorCountry: "",
      registerFormErrorGender: "",
      registerFormErrorAge: "",
      registerFormErrorProfileImage: "",
      registerFormErrorUserTNC: "",
      registerFormErrorTime: "",
      registerFormErrorPhoneNumber: "",
      errorChooseServices: "",
      errorChooseType: "",
      userProfileImageLable: "Choose image",
      userName: "",
      userEmail: "",
      userPassword: "",
      userAddress: "",
      userPincode: "",
      userConfirmPassword: false,
      userCity: "",
      userCountry: "",
      userGender: "Male",
      userAge: "",
      userProfileImage: null,
      userTNC: false,
      // showErrorTimeMonday: false,
      // showErrorTimeTuesday: false,
      // showErrorTimeWednesday: false,
      // showErrorTimeThursday: false,
      // showErrorTimeFriday: false,
      // showErrorTimeSaturday: false,
      // showErrorTimeSunday: false,

      
      phoneNumber: "",
      userLoginEmail: "",
      userLoginPassword: "",
      choosetype: "",
      chooseservice: [],
      opentime: "",
      closetime: "",
      handletime: [],
    };

    this.handleUserName = this.handleUserName.bind(this);
    this.handleUserEmail = this.handleUserEmail.bind(this);
    this.handleUserAddress = this.handleUserAddress.bind(this);
   this.handleUserPincode=this.handleUserPincode.bind(this);
   this.handletimefunc=this.handletimefunc.bind(this);


    this.handleUserPassword = this.handleUserPassword.bind(this);
    this.handleUserConfirmPassword = this.handleUserConfirmPassword.bind(this);
    this.handleUserCity = this.handleUserCity.bind(this);
    this.handleUserCountry = this.handleUserCountry.bind(this);
    this.handleUserAge = this.handleUserAge.bind(this);
    this.handleCreateAccountBtn = this.handleCreateAccountBtn.bind(this);
    this.handleUserProfileImage = this.handleUserProfileImage.bind(this);
    this.handleUserTNC = this.handleUserTNC.bind(this);
    this.handlePhoneNumber = this.handlePhoneNumber.bind(this);
    this.handleUserGender = this.handleUserGender.bind(this);
   

  }

  handleForms() {
    //ChangeAJP
    this.props.history.push("/login");
  }

  handleUserName(e) {
    const userName = e;
    const userNameFormate = /^([A-Za-z.\s_-]).{2,}$/;
    if (userName.match(userNameFormate)) {
      this.setState({
        showErrorUsername: false,
        registerFormErrorUsername: "",
        userName: userName,
      });
    } else {
      this.setState({
        showErrorUsername: true,
        registerFormErrorUsername: "Please enter a valid name.",
        userName: "",
      });
    }
  }

  handleUserEmail(e) {
    const userEmail = e;
    const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (userEmail.match(userEmailFormate)) {
      this.setState({
        showErrorEmail: false,
        registerFormErrorEmail: "",
        userEmail: userEmail,
      });
    } else {
      this.setState({
        showErrorEmail: true,
        registerFormErrorEmail: "Please enter a valid email address.",
        userEmail: "",
      });
    }
  }

  handleUserPassword(e) {
    const userPassword = e;
    const userPasswordFormate = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{10,}/;
    if (userPassword.match(userPasswordFormate)) {
      this.setState({
        showErrorPassword: false,
        registerFormErrorPassword: "",
        userPassword: userPassword,
      });
    } else {
      this.setState({
        showErrorPassword: true,
        registerFormErrorPassword:
          "Use alphanumeric, uppercase, lowercase & greater than 10 characters.",
        userPassword: "",
      });
    }
  }

  handleUserConfirmPassword(e) {
    const userConfirmPassword = e;
    const { userPassword } = this.state;
    if (userConfirmPassword.match(userPassword)) {
      this.setState({
        showErrorCPassword: false,
        registerFormErrorCPassword: "",
        userConfirmPassword: true,
      });
    } else {
      this.setState({
        showErrorCPassword: true,
        registerFormErrorCPassword: "Confirmation password not matched.",
        userConfirmPassword: false,
      });
    }
  }

  handleUserCity(e) {
    const userCity = e;
    const userCityFormate = /^([A-Za-z.\s_-]).{2,}$/;
    if (userCity.match(userCityFormate)) {
      this.setState({
        showErrorCity: false,
        registerFormErrorCity: "",
        userCity: userCity,
      });
    } else {
      this.setState({
        showErrorCity: true,
        registerFormErrorCity: "Please enter a valid city name.",
        userCity: "",
      });
    }
  }

  handleUserCountry(e) {
    const userCountry = e;
    const userCountryFormate = /^([A-Za-z.\s_-]).{2,}$/;
    if (userCountry.match(userCountryFormate)) {
      this.setState({
        showErrorCountry: false,
        registerFormErrorCountry: "",
        userCountry: userCountry,
      });
    } else {
      this.setState({
        showErrorCountry: true,
        registerFormErrorCountry: "Please enter a valid country name.",
        userCountry: "",
      });
    }
  }
  handleUserAddress(e) {
    const userAddress = e;
    
      this.setState({
        showErrorCountry: false,
        registerFormErrorAddress: "",
        userAddress: userAddress,
      });
     
    }
  
  handleUserGender(e) {
    this.setState({
      userGender: e.target.value,
    });
  }

  handleUserAge(e) {
    const userAge = e;
    if (userAge > 0 && userAge < 101) {
      this.setState({
        showErrorAge: false,
        registerFormErrorAge: "",
        userAge: userAge,
      });
    } else {
      this.setState({
        showErrorAge: true,
        registerFormErrorAge: "Please enter a valid age.",
        userAge: "",
      });
    }
  }
  handleUserPincode(e) {
    const userPincode = e;
    if (userPincode.length === 6) {
      this.setState({
        showErrorPincode: false,
        registerFormErrorPincode: "",
        userPincode: userPincode,
      });
    } else {
      this.setState({
        showErrorPincode: true,
        registerFormErrorPincode: "Please enter a valid Pincode.",
        userPincode: "",
      });
    }
  }

  handleUserProfileImage(e) {
    const userimage = e;

    if (userimage != null) {
      this.setState({
        showErrorProfileImage: false,
        registerFormErrorProfileImage: "",
        userProfileImageLable: e.target.files[0].name,
        userProfileImage: e.target.files[0],
      });
    } else {
      this.setState({
        showErrorProfileImage: true,
        registerFormErrorProfileImage: "Please select a profile image.",
        userProfileImageLable: "Choose image...",
        userProfileImage: "",
      });
    }
  }

  handlePhoneNumber(e) {
    const phoneNumber = e;
    const userPhoneNumberFormate = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
    if (phoneNumber.match(userPhoneNumberFormate)) {
      this.setState({
        showErrorPhoneNumber: false,
        registerFormErrorPhoneNumber: "",
        phoneNumber: phoneNumber,
      });
    } else {
      this.setState({
        showErrorPhoneNumber: true,
        registerFormErrorPhoneNumber: "Please enter a valid phone number.",
        phoneNumber: "",
      });
    }
  }
  handledays() {}

  
  handleUserTNC() {
    const { userTNC } = this.state;
    if (!userTNC) {
      this.setState({
        userTNC: true,
        showErrorUserTNC: false,
        registerFormErrorUserTNC: "",
      });
    } else {
      this.setState({
        userTNC: false,
        showErrorUserTNC: true,
        registerFormErrorUserTNC: "Please accept terms and conditions.",
      });
    }
  }
  handletimefunc(day)
  {
   //const handletime=e;
   const{handletime}=this.state;
   //const count=0;
   if(handletime)
   {
     if(day==="1")
     {
       console.log(day)
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeMonday: true,
          showErrorTimeTuesday: false,
          showErrorTimeWednesday: false,
          showErrorTimeThursday: false,
          showErrorTimeFriday: false,
          showErrorTimeSaturday: false,
          showErrorTimeSunday: false,
          

          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
    })
  }
  if(day==="2")
     {
       console.log("day",day);
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeTuesday: true,
          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
    })
  }
  if(day==="3")
     {
      
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeWednesday: true,
          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
      
    })
    // console.log("showerrortype",showErrorTimeWednesday)
  }
  if(day==="4")
  {
 handletime.map((val)=>{

 if(handletime[1]<handletime[0])
   {
     
     this.setState({
       showErrorTimeThursday: true,
       registerFormErrorTime: "Endtime should be greater them the starting time .",
       handletime: [],
      
     });
   }
 })
}
if(day==="5")
     {
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeFriday: true,
          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
    })
  }
  if(day==="6")
     {
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeSaturday: true,
          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
    })
  }
  if(day==="7")
     {
    handletime.map((val)=>{
   
    if(handletime[1]<handletime[0])
      {
        
        this.setState({
          showErrorTimeSunday: true,
          registerFormErrorTime: "Endtime should be greater them the starting time .",
          handletime: [],
         
        });
      }
    })
  }
}
  
      //console.log("count",count);

  }

  async handleCreateAccountBtn(e) {
    const {
      userName,
      userEmail,
      userPassword,
      userConfirmPassword,
      userCity,
      userCountry,
      userAddress,
      userPincode,
      userGender,
      userAge,
      userProfileImage,
      userTNC,
      phoneNumber,
      chooseservice,
      choosetype,
      handletime,
    } = this.state;
    console.log("name is",e.name);
    // const whiteSpaces = /^(?!\s*$)[-a-zA-Z0-9_:,.' ']{1,100}$/;
    const userNameFormate = /^([A-Za-z.\s_-]).{2,}$/;
    const userEmailFormate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const userPasswordFormate = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{10,}/;
    const userCountryFormate = /^([A-Za-z.\s_-]).{2,}$/;
    const userCityFormate = /^([A-Za-z.\s_-]).{2,}$/;

    if (!userName.match(userNameFormate)) {
      this.setState({
        showErrorUsername: true,
        registerFormErrorUsername: "Please enter a valid name.",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userEmail.match(userEmailFormate)) {
        this.setState({
          showErrorEmail: true,
          registerFormErrorEmail: "Please enter a valid email address.",
          userEmail: "",
        });
        if (!userPassword.match(userPasswordFormate)) {
          this.setState({
            showErrorPassword: true,
            registerFormErrorPassword:
              "Use alphanumeric, uppercase, lowercase & greater than 10 characters.",
            userPassword: "",
          });
          if (!userCity.match(userCityFormate)) {
            this.setState({
              showErrorCity: true,
              registerFormErrorCity: "Please enter a valid city name.",
              userCity: "",
            });
            if (!userCountry.match(userCountryFormate)) {
              this.setState({
                showErrorCountry: true,
                registerFormErrorCountry: "Please enter a valid country name.",
                userCountry: "",
              });
              if (!(userAge > 0 && userAge < 101)) {
                this.setState({
                  showErrorAge: true,
                  registerFormErrorAge: "Please enter a valid age.",
                  userAge: "",
                });
                if (userProfileImage == null) {
                  // this.setState({
                  //   showErrorProfileImage: true,
                  //   registerFormErrorProfileImage:
                  //     "Please select a profile image.",
                  //   userProfileImageLable: "Choose image...",
                  //   userProfileImage: "",
                  // });
                  if (phoneNumber.length == 0) {
                    this.setState({
                      showErrorPhoneNumber: true,
                      registerFormErrorPhoneNumber: "Phone number is required",
                    });
                    if (chooseservice.length == 0 ) {
                      this.setState({
                        showErrorChooseServices: true,
                        errorChooseServices: "Services field is required",
                      });
                      if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
                      if (!userAddress) {
                        this.setState({
                          showErrorAddress: true,
                          registerFormErrorAddress: "Please enter an Address",
                        });
                      }
                      if (!choosetype) {
                        this.setState({
                          showErrorChoosetype: true,
                          errorChooseType: "Item type is required",
                        });
                        if (!userTNC) {
                          this.setState({
                            userTNC: false,
                            showErrorUserTNC: true,
                            registerFormErrorUserTNC:
                              "Please accept terms and conditions.",
                          });
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (!userEmail.match(userEmailFormate)) {
      this.setState({
        showErrorEmail: true,
        registerFormErrorEmail: "Please enter a valid email address.",
        userEmail: "",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!userPassword.match(userPasswordFormate)) {
        this.setState({
          showErrorPassword: true,
          registerFormErrorPassword:
            "Use alphanumeric, uppercase, lowercase & greater than 10 characters.",
          userPassword: "",
        });
        if (!userCity.match(userCityFormate)) {
          this.setState({
            showErrorCity: true,
            registerFormErrorCity: "Please enter a valid city name.",
            userCity: "",
          });
          if (!userCountry.match(userCountryFormate)) {
            this.setState({
              showErrorCountry: true,
              registerFormErrorCountry: "Please enter a valid country name.",
              userCountry: "",
            });
            if (!(userAge > 0 && userAge < 101)) {
              this.setState({
                showErrorAge: true,
                registerFormErrorAge: "Please enter a valid age.",
                userAge: "",
              });
              if (userProfileImage == null) {
                // this.setState({
                //   showErrorProfileImage: true,
                //   registerFormErrorProfileImage:
                //     "Please select a profile image.",
                //   userProfileImageLable: "Choose image...",
                //   userProfileImage: "",
                // });
                if (phoneNumber.length == 0) {
                  this.setState({
                    showErrorPhoneNumber: true,
                    registerFormErrorPhoneNumber: "Phone number is required",
                  });
                  if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
                  if (!chooseservice) {
                    this.setState({
                      showErrorChooseServices: true,
                      errorChooseServices: "Services field is required",
                    });
                    if (!choosetype) {
                      this.setState({
                        showErrorChoosetype: true,
                        errorChooseType: "Item type is required",
                      });
                      if (!userTNC) {
                        this.setState({
                          userTNC: false,
                          showErrorUserTNC: true,
                          registerFormErrorUserTNC:
                            "Please accept terms and conditions.",
                        });
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (!userPassword.match(userPasswordFormate)) {
      this.setState({
        showErrorPassword: true,
        registerFormErrorPassword:
          "Use alphanumeric, uppercase, lowercase & greater than 10 characters.",
        userPassword: "",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!userCity.match(userCityFormate)) {
        this.setState({
          showErrorCity: true,
          registerFormErrorCity: "Please enter a valid city name.",
          userCity: "",
        });
        if (!userCountry.match(userCountryFormate)) {
          this.setState({
            showErrorCountry: true,
            registerFormErrorCountry: "Please enter a valid country name.",
            userCountry: "",
          });
          if (!(userAge > 0 && userAge < 101)) {
            this.setState({
              showErrorAge: true,
              registerFormErrorAge: "Please enter a valid age.",
              userAge: "",
            });
            if (userProfileImage == null) {
              // this.setState({
              //   showErrorProfileImage: true,
              //   registerFormErrorProfileImage: "Please select a profile image.",
              //   userProfileImageLable: "Choose image...",
              //   userProfileImage: "",
              // });
              if (phoneNumber.length == 0) {
                this.setState({
                  showErrorPhoneNumber: true,
                  registerFormErrorPhoneNumber: "Phone number is required",
                });
                if (!chooseservice) {
                  this.setState({
                    showErrorChooseServices: true,
                    errorChooseServices: "Services field is required",
                  });
                  if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
                  if (!choosetype) {
                    this.setState({
                      showErrorChoosetype: true,
                      errorChooseType: "Item type is required",
                    });
                    if (!userTNC) {
                      this.setState({
                        userTNC: false,
                        showErrorUserTNC: true,
                        registerFormErrorUserTNC:
                          "Please accept terms and conditions.",
                      });
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (!userCity.match(userCityFormate)) {
      this.setState({
        showErrorCity: true,
        registerFormErrorCity: "Please enter a valid city name.",
        userCity: "",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!userCountry.match(userCountryFormate)) {
        this.setState({
          showErrorCountry: true,
          registerFormErrorCountry: "Please enter a valid country name.",
          userCountry: "",
        });
        if (!(userAge > 0 && userAge < 101)) {
          this.setState({
            showErrorAge: true,
            registerFormErrorAge: "Please enter a valid age.",
            userAge: "",
          });
          if (userProfileImage == null) {
            // this.setState({
            //   showErrorProfileImage: true,
            //   registerFormErrorProfileImage: "Please select a profile image.",
            //   userProfileImageLable: "Choose image...",
            //   userProfileImage: "",
            // });
            if (phoneNumber.length == 0) {
              this.setState({
                showErrorPhoneNumber: true,
                registerFormErrorPhoneNumber: "Phone number is required",
              });
              if (!chooseservice) {
                this.setState({
                  showErrorChooseServices: true,
                  errorChooseServices: "Services field is required",
                });
                if (!choosetype) {
                  this.setState({
                    showErrorChoosetype: true,
                    errorChooseType: "Item type is required",
                  });
                  if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
                  if (!userTNC) {
                    this.setState({
                      userTNC: false,
                      showErrorUserTNC: true,
                      registerFormErrorUserTNC:
                        "Please accept terms and conditions.",
                    });
                  }
                }
              }
            }
          }
        }
      }
    }
    if (!userCountry.match(userCountryFormate)) {
      this.setState({
        showErrorCountry: true,
        registerFormErrorCountry: "Please enter a valid country name.",
        userCountry: "",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!(userAge > 0 && userAge < 101)) {
        this.setState({
          showErrorAge: true,
          registerFormErrorAge: "Please enter a valid age.",
          userAge: "",
        });
        if (userProfileImage == null) {
          // this.setState({
          //   showErrorProfileImage: true,
          //   registerFormErrorProfileImage: "Please select a profile image.",
          //   userProfileImageLable: "Choose image...",
          //   userProfileImage: "",
          // });
          if (phoneNumber.length == 0) {
            this.setState({
              showErrorPhoneNumber: true,
              registerFormErrorPhoneNumber: "Phone number is required",
            });
            if (!chooseservice) {
              this.setState({
                showErrorChooseServices: true,
                errorChooseServices: "Services field is required",
              });
              if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
              if (!choosetype) {
                this.setState({
                  showErrorChoosetype: true,
                  errorChooseType: "Item type is required",
                });
                if (!userTNC) {
                  this.setState({
                    userTNC: false,
                    showErrorUserTNC: true,
                    registerFormErrorUserTNC:
                      "Please accept terms and conditions.",
                  });
                }
              }
            }
          }
        }
      }
    }

    if (!(userAge > 0 && userAge < 101)) {
      this.setState({
        showErrorAge: true,
        registerFormErrorAge: "Please enter a valid age.",
        userAge: "",
      });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (userProfileImage == null) {
        // this.setState({
        //   showErrorProfileImage: true,
        //   registerFormErrorProfileImage: "Please select a profile image.",
        //   userProfileImageLable: "Choose image...",
        //   userProfileImage: "",
        // });
        if (phoneNumber.length == 0) {
          this.setState({
            showErrorPhoneNumber: true,
            registerFormErrorPhoneNumber: "Phone number is required",
          });
          if (!chooseservice) {
            this.setState({
              showErrorChooseServices: true,
              errorChooseServices: "Services field is required",
            });
            if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
            if (!choosetype) {
              this.setState({
                showErrorChoosetype: true,
                errorChooseType: "Item type is required",
              });
              if (!userTNC) {
                this.setState({
                  userTNC: false,
                  showErrorUserTNC: true,
                  registerFormErrorUserTNC:
                    "Please accept terms and conditions.",
                });
              }
            }
          }
        }
      }
    }
    if (userProfileImage == null) {
      // this.setState({
      //   showErrorProfileImage: true,
      //   registerFormErrorProfileImage: "Please select a profile image.",
      //   userProfileImageLable: "Choose image...",
      //   userProfileImage: "",
      // });
      if (chooseservice.length == 0 ) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
      }
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (phoneNumber.length == 0) {
        this.setState({
          showErrorPhoneNumber: true,
          registerFormErrorPhoneNumber: "Phone number is required",
        });
        if (!chooseservice) {
          this.setState({
            showErrorChooseServices: true,
            errorChooseServices: "Services field is required",
          });
          if (!choosetype) {
            this.setState({
              showErrorChoosetype: true,
              errorChooseType: "Item type is required",
            });
            if (!userAddress) {
              this.setState({
                showErrorAddress: true,
                registerFormErrorAddress: "Please enter an Address",
              });
            }
            if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
            if (!userTNC) {
              this.setState({
                userTNC: false,
                showErrorUserTNC: true,
                registerFormErrorUserTNC: "Please accept terms and conditions.",
              });
            }
          }
        }
      }
    }
    if (phoneNumber.length == 0) {
      this.setState({
        showErrorPhoneNumber: true,
        registerFormErrorPhoneNumber: "Phone number is required",
      });
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!chooseservice) {
        this.setState({
          showErrorChooseServices: true,
          errorChooseServices: "Services field is required",
        });
        if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
        if (!choosetype) {
          this.setState({
            showErrorChoosetype: true,
            errorChooseType: "Item type is required",
          });
          if (!userTNC) {
            this.setState({
              userTNC: false,
              showErrorUserTNC: true,
              registerFormErrorUserTNC: "Please accept terms and conditions.",
            });
          }
        }
      }
    }
    if (chooseservice.length == 0 ) {
      this.setState({
        showErrorChooseServices: true,
        errorChooseServices: "Services field is required",
      });
    }
    if (!chooseservice) {
      this.setState({
        showErrorChooseServices: true,
        errorChooseServices: "Services field is required",
      });
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
      if (!choosetype) {
        this.setState({
          showErrorChoosetype: true,
          errorChooseType: "Item type is required",
        });
        if(userPincode.length== 0)
                      {
                        this.setState({
                          showErrorPincode: true,
                          registerFormErrorPincode: "Pincode is  is required",
                        });
                      }
        if (!userTNC) {
          this.setState({
            userTNC: false,
            showErrorUserTNC: true,
            registerFormErrorUserTNC: "Please accept terms and conditions.",
          });
        }
      }
    }
    if (!choosetype) {
      this.setState({
        showErrorChoosetype: true,
        errorChooseType: "Item type is required",
      });
      if (!userTNC) {
        this.setState({
          userTNC: false,
          showErrorUserTNC: true,
          registerFormErrorUserTNC: "Please accept terms and conditions.",
        });
      }
    }
    if (!userTNC) {
      this.setState({
        userTNC: false,
        showErrorUserTNC: true,
        registerFormErrorUserTNC: "Please accept terms and conditions.",
      });
    } else if (!userConfirmPassword) {
      this.setState({
        showErrorCPassword: true,
        registerFormErrorCPassword: "Confirmation password not matched.",
        userConfirmPassword: false,
      });
      if (!userAddress) {
        this.setState({
          showErrorAddress: true,
          registerFormErrorAddress: "Please enter an Address",
        });
      }
    } else {
      // console.log(userName, userEmail, userPassword, userConfirmPassword, userCity, userCountry, userGender, userAge, userProfileImage, userTNC)
      const userDetails = {
        userName: userName,
        userEmail: userEmail,
        userPassword: userPassword,
        userCity: userCity,
        userPincode: userPincode,
        userAddress: userAddress,
        userCountry: userCountry,
        userGender: userGender,
        userAge: userAge,
        userProfileImage: userProfileImage,
        phoneNumber: phoneNumber,
        chooseservice: chooseservice,
        choosetype: choosetype,
        isRestaurant: true,
        propsHistory: this.props.history,
        typeOfFood: ["Apple Juice", "Chicken Roast", "Cheese Burger"],
      };
      try {
        const signUpReturn = await signUp(userDetails);
        // console.log(signUpReturn)
      } catch (error) {
        console.log("Error in Register Restaurant => ", error);
        this.setState({
          showError: true,
          registerFormError: "Email is already taken",
        });
      }
    }
  }


  render() {
    const {
      showError,
      showErrorUsername,
      showErrorEmail,
      showErrorPassword,
      showErrorCPassword,
      showErrorCity,
      showErrorTimeMonday,
      showErrorTimeTuesday,
      showErrorTimeWednesday,
      showErrorTimeThursday,
      showErrorTimeFriday,
      showErrorTimeSaturday,
      showErrorTimeSunday,
      showErrorAddress,
      showErrorPincode,
      showErrorCountry,
      showErrorAge,
      showErrorProfileImage,
      showErrorPhoneNumber,
      showErrorChooseServices,
      showErrorChoosetype,
      showErrorUserTNC,
      registerFormError,
      registerFormErrorAddress,
      registerFormErrorPincode,
      registerFormErrorTime,
      registerFormErrorUsername,
      registerFormErrorEmail,
      registerFormErrorPassword,
      registerFormErrorCPassword,
      registerFormErrorCity,
      registerFormErrorCountry,
      registerFormErrorAge,
      registerFormErrorProfileImage,
      registerFormErrorPhoneNumber,
      registerFormErrorUserTNC,
      errorChooseServices,
      errorChooseType,
      userProfileImageLable,
      userTNC,
      userGender,
    } = this.state;
    return (
      <div>
        <div className="container-fluid register-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />
            <div className="container register-cont1-text">
              <h1 className="text-uppercase text-white text-center mb-4">
                <strong>Register User And Add Restaurant</strong>
              </h1>
            </div>
          </div>
        </div>
        <div className="container-fluid py-5 bg-light">
          <div className="col-lg-6 col-md-6 col-sm-12 mx-auto bg-white shadow p-4">
            <h2 className="text-center mb-4">Register Restaurant</h2>
            <form action="javascript:void(0)">
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="userFullName"><b>Restaurant Name</b></label>
                  <input
                    type="text"
                    className="form-control"
                    id="userName"
                    placeholder="Restaurant Name"
                    onKeyUp={(e) => this.handleUserName(e.target.value)}
                  />
                  {showErrorUsername ? <small className="text-danger mb-0">{registerFormErrorUsername}</small> : null}
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="userEmail"><b>Email</b></label>
                  <input
                    type="email"
                    autoComplete="false"
                    className="form-control"
                    id="userEmail"
                    placeholder="Email"
                    onKeyUp={(e) => this.handleUserEmail(e.target.value)}
                  />
                  
                  {showErrorEmail ? <small className="text-danger mb-0">{registerFormErrorEmail}</small> : null}
                  {showError ? <small className="text-danger mb-0">{registerFormError}</small> : null}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="userPassword"><b>Password</b></label>
                  <input
                    type="password"
                    autoComplete="new-password"
                    className="form-control"
                    id="userPassword"
                    placeholder="Password"
                    onKeyUp={(e) => this.handleUserPassword(e.target.value)}
                  />
                  {showErrorPassword ? <small className="text-danger mb-0">{registerFormErrorPassword}</small> : null}
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="userConfirmPassword"><b>Confirm Password</b></label>
                  <input
                    type="password"
                    className="form-control"
                    id="userConfirmPassword"
                    placeholder="Password"
                    onKeyUp={(e) =>
                      this.handleUserConfirmPassword(e.target.value)
                    }
                  />
                  {showErrorCPassword ? <small className="text-danger mb-0">{registerFormErrorCPassword}</small> : null}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="userAddress"><b>Address</b></label>
                  <input
                    type="text"
                    max="40"
                    className="form-control"
                    id="userAddress"
                    placeholder="Address"
                    onKeyUp={(e) => this.handleUserAddress(e.target.value)}
                  />
                  {showErrorAddress ? <small className="text-danger mb-0">{registerFormErrorAddress}</small> : null}
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="userCity"><b>City</b></label>
                  <input
                    type="text"
                    className="form-control"
                    id="userCity"
                    placeholder="City"
                    onKeyUp={(e) => this.handleUserCity(e.target.value)}
                  />
                  {showErrorCity ? <small className="text-danger mb-0">{registerFormErrorCity}</small> : null}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="userCountry"><b>Country</b></label>
                  <input
                    type="text"
                    className="form-control"
                    id="userCountry"
                    placeholder="Country"
                    onKeyUp={(e) => this.handleUserCountry(e.target.value)}
                  />
                  {showErrorCountry ? <small className="text-danger mb-0">{registerFormErrorCountry}</small> : null}
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="userPincode"><b>Pincode</b></label>
                  <input
                    type="number"
                    className="form-control"
                    id="userPincode"
                    placeholder="Pincode"
                    onKeyUp={(e) => this.handleUserPincode(e.target.value)}
                  />
                  {showErrorPincode ? <small className="text-danger mb-0">{registerFormErrorPincode}</small> : null}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-4">
                  <label htmlFor="userGender"><b>Gender</b></label>
                  <select
                    id="userGender"
                    className="form-control"
                    value={userGender}
                    onChange={this.handleUserGender}
                  >
                    <option defaultValue>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div className="form-group col-md-2">
                  <label htmlFor="userAge"><b>Age</b></label>
                  <input
                    type="number"
                    className="form-control"
                    id="userAge"
                    onKeyUp={(e) => this.handleUserAge(e.target.value)}
                  />
                  {showErrorAge ? <small className="text-danger mb-0">{registerFormErrorAge}</small> : null}
                </div>
                <div className="form-group col-md-6">
                  <p className="mb-2"><b>Profile Image</b></p>
                  <div className="custom-file">
                    <input
                      type="file"
                      className="custom-file-input"
                      id="userProfileImage"
                      accept="image/x-png,image/gif,image/jpeg"
                      onChange={this.handleUserProfileImage}
                    />
                    <label
                      className="custom-file-label"
                      htmlFor="userProfileImage"
                    >
                      {userProfileImageLable}
                    </label>
                    {showErrorProfileImage ? <small className="text-danger mb-0">{registerFormErrorProfileImage}</small> : null}
                  </div>
                </div>
              </div>
              {/* phone number */}
              <div className="form-group">
                <label htmlFor="phonenumber"><b>Phone Number</b></label>
                <input
                  type="text"
                  className="form-control"
                  id="phonenumber"
                  placeholder="Phone Number"
                  onKeyUp={(e) => this.handlePhoneNumber(e.target.value)}
                />
                {showErrorPhoneNumber ? <small className="text-danger mb-0">{registerFormErrorPhoneNumber}</small> : null}
              </div>
              {/* phone number */}
              {/* added checkbox */}
              <label className="mb-2"><b>Services</b></label>
              
               <div className="form-row">
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Breakfast"
                      value="BreakFast"
                      name="Breakfast"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)}
                      
                    />
                    <label className="custom-control-label" htmlFor="Breakfast">
                      Breakfast
                    </label>
                  </div>
                </div>
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Lunch"
                      value="Lunch"
                      name="Lunch"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)}
                     
                    />
                    <label className="custom-control-label" htmlFor="Lunch">
                      Lunch
                    </label>
                  </div>
                </div>
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Dinner"
                      value="Dinner"
                      name="Dinner"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)
                       }
                      
                     
                    />
                    <label className="custom-control-label" htmlFor="Dinner">
                      Dinner
                    </label>
                  </div>
                </div>
                </div>
                <div className="form-row">
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Snacks"
                      value="Snacks"
                      name="Snacks"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)}
                   
                    />
                    <label className="custom-control-label" htmlFor="Snacks">
                      Snacks
                    </label>
                  </div>
                </div>
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Drinks"
                      value="Drinks"
                      name="Drinks"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)}
                      
                    />
                    <label className="custom-control-label" htmlFor="Drinks">
                      Drinks
                    </label>
                  </div>
                </div>
                <div className="form-group col">
                  <div className="custom-control custom-radio">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="Dessert"
                      value="Dessert"
                      name="Dessert"
                      onChange={(e) =>
                        this.state.chooseservice.push(e.target.value)}
                      
                    />
                    <label className="custom-control-label" htmlFor="Dessert">
                      Dessert
                    </label>
                  </div>
                </div>
                
              </div> 
              {showErrorChooseServices ? <small className="text-danger mb-0">{errorChooseServices}</small> : null}
              {/* choose type */}
              
              <div className="form-group">
                <label className="mb-2"><b>Choose Type</b></label>
                
                <div className="form-column">
                  <div className="row">{/*Start of 1st row */}
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Veg"
                        value="Veg"
                        name="Veg"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Veg">
                        Veg
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Non-Veg"
                        value="Non-Veg"
                        name="Non-Veg"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Non-Veg">
                        Non-Veg
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="VegandNon-Veg"
                        value="VegandNon-Veg"
                        name="VegandNon-Veg"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="VegandNon-Veg"
                      >
                        Veg & Non-Veg
                      </label>
                    </div>
                    </div>
                    </div>{/*End of 1st row */}

                  <div className="row">{/*Start of 2nd row */}
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Juice"
                        value="Juice"
                        name="Juice"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Juice">
                        Juice
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Desserts"
                        value="Desserts"
                        name="Desserts"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Desserts">
                        Dessert
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                   
                   
                      <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Special"
                        value="Special"
                        name="Special"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Special">
                        Special
                      </label>
                    </div>
                  </div>
                  </div>{/*End of 2nd row */}
                 
                  <div className="row">{/*Start of 3rd row */}
                    <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Chinese"
                        value="Chinese"
                        name="Chinese"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Chinese">
                        Chinese
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Italian"
                        value="Italian"
                        name="Italian"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Italian">
                        Italian
                      </label>
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Contidental"
                        value="Contidental"
                        name="Drinks"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="Contidental"
                      >
                        Contidental
                      </label>
                    </div>
                  </div>
                        </div>{/*End of 3rd row */}
                    
                  <div className="row">{/*Start of 4th row */}
                  <div className="form-group col-md-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="Others"
                        value="Others"
                        name="Others"
                        onChange={(e) =>
                          this.setState({ choosetype: e.target.value })
                        }
                      />
                      <label className="custom-control-label" htmlFor="Others">
                        Others
                      </label>
                    </div>
                  </div>
                  </div>{/*End of 4th row */}
                  </div>
              </div>
              
              
              {showErrorChoosetype ? <small className="text-danger mb-0">{errorChooseType}</small> : null}

              {/* end */}
              {/* <label className="RestaurantOpen">Restaurant Open</label> */}

              {/*<div className="form-row">
                <div className="form-group col-md-6">
                  <div className="form-group">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        className="custom-control-checkbox"
                      />
                      <label className="custom-control-input" htmlFor="Days">
                        Monday
                      </label>
                      <span className="OpenTime">
                        <input type="time" />
                      </span>
                    </div>
                  </div>
                </div>
              </div>*/}
              <label className="RestaurantOpen"><b>Restaurant Timings</b></label>
              <div className="timings">
                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox1"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox1"
                      >
                        Monday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime" 
                     
                      onChange={(e)=>this.state.handletime.push(e.target.value)}/>{" "}
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                      onChange={(e)=>(this.state.handletime.push(e.target.value) && this.handletimefunc("1"))}/>
                      
                      
                       
                       {showErrorTimeMonday ? <small className="text-danger mb-0">{registerFormErrorTime}</small> :null}
                       {console.log("monday",showErrorTimeMonday)}
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox2"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox2"
                      >
                        Tuesday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)} />{" "}
                      
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                     onChange={(e)=>this.state.handletime.push(e.target.value) && this.handletimefunc("2")}/>
                       
                      {showErrorTimeTuesday ?  <small className="text-danger mb-0">{registerFormErrorTime}</small> : null }
                      {console.log("monday",showErrorTimeTuesday)}
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox3"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox3"
                      >
                        Wednesday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime" 
                      onChange={(e)=>this.state.handletime.push(e.target.value)}/>{" "}
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                      onChange={(e)=>this.state.handletime.push(e.target.value) && this.handletimefunc("3")}/>
                      {this.handletimefunc("3")}
                      
                       
                       {showErrorTimeWednesday ? <small className="text-danger mb-0">{registerFormErrorTime}</small> : null}
                        
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox4"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox4"
                      >
                        Thursday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)} />{" "}
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime" 
                      onChange={(e)=>this.state.handletime.push(e.target.value) && this.handletimefunc("4")}/>
                      
                      
                       
                       {showErrorTimeThursday ? <small className="text-danger mb-0">{registerFormErrorTime}</small> : null}
                        
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox5"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox5"
                      >
                        Friday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)} />{" "}
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                      onChange={(e)=>this.state.handletime.push(e.target.value) && this.handletimefunc("5")}/>
                       
                       {showErrorTimeFriday ? <small className="text-danger mb-0">{registerFormErrorTime}</small> : null}
                        
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox6"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox6"
                      >
                        Saturday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)} />{" "}
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                     onChange={(e)=>this.state.handletime.push(e.target.value) && this.handletimefunc("6")}/>
                      
                       
                       {showErrorTimeSaturday? <small className="text-danger mb-0">{registerFormErrorTime}</small> : null}
                        
                    </span>
                  </div>
                </div>

                <div className="form-row1">
                  <div className="form-group col-md-111">
                    <div className="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        id="checkbox7"
                        className="custom-control-input"
                        onChange={this.handledays}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="checkbox7"
                      >
                        Sunday
                      </label>{" "}
                    </div>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="opentime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)}/>
                        
                    </span>
                  </div>
                  <div className="form-group col-md-111">
                    <span>
                      <input type="time" id="closetime"
                      onChange={(e)=>this.state.handletime.push(e.target.value)}/>
                      {this.handletimefunc("7")}
                      
                       
                       {showErrorTimeSunday ? <small className="text-danger mb-0">{registerFormErrorTime}</small> : null} 
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="userTNC"
                    defaultChecked={userTNC}
                    onChange={this.handleUserTNC}
                  />
                  <label className="custom-control-label" htmlFor="userTNC">
                    Accept Terms and Condition
                  </label>
                </div>
                {showErrorUserTNC ? <small className="text-danger mb-0">{registerFormErrorUserTNC}</small> : null}
              </div>

              {/* <p className="text-danger">{showError ? registerFormError : null}</p> */}
              <button
                type="submit"
                className="btn btn-warning text-uppercase mb-3"
                onClick={this.handleCreateAccountBtn}
              >
                <b>Create an Account</b>
              </button>
            </form>
            {/* ChangeAJP */}
            <p className="m-0">
              Already have an account?{" "}
              <span
                className="cursor-pointer text-warning"
                onClick={() => this.handleForms()}
              >
                Login Here
              </span>
            </p>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
